This codebase runs off a generated HTML page that is populated with dynamic data pertinent to the game that is being profiled.

That data is added and updated via a separate, Laravel-based codebase.

#### Motivation
There's a market demand for information websites that can show the gaming enthusiast an accurate idea of what their latency is to their favourite game's online servers.

####Execution

The app uses three state trees:
- UI (For user interface-specific features)
- Servers (data on servers to be checked)
- Pings (data on latency checks for the servers)

Data is stored in local storage on a per-game basis. On mounting, this local storage is checked for and loaded in to state if it exists.

A library of helper functions handles latency checking and other requirements.

####SEO
Consideration was given to as much of the code being statically generated as possible to give Google some clear canonical content to work with.

####Redux
Action creators and types are kept in the `src/actions` folder, reducers in the `srsc/reducers` folder, combined in `index.js`

####Styling and SASS
SCSS functionality was added via the `sass-loader` plugin added to both dev and prod webpack configs under `/config`
The app really only demonstrated use of importing, nesting and variables, but no `@extend`-ing or operators.
The framework was based on "Grommet" (version 1) https://v1.grommet.io/ Grommet version two has just been released and is much more suited to theming, using styled components.