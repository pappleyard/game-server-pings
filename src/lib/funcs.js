import moment from "moment-timezone"

const indexColors = {
	"ping-50": "rgb(143, 204, 51)",
	"ping-100": "rgb(181, 204, 51)",
	"ping-150": "rgb(204, 189, 51)",
	"ping-200": "rgb(204, 150, 51)",
	"ping-250": "rgb(204, 112, 51)",
	"ping-300": "rgb(204, 51, 51)"
}

/**
 * Creates and loads an image element by url.
 * @param  {String} url
 * @return {Promise} promise that resolves to an image element or
 *                   fails to an Error.
 */
const request_image = (url, timeout = 5000) =>
{
	return new Promise((resolve, reject) =>
	{
		let tko = setTimeout(() =>
		{
			reject("Timed out")
		}, timeout)
		
		var img = new Image()
		img.onload = function ()
		{
			clearTimeout(tko)
			resolve(img)
		}
		img.onerror = function ()
		{
			resolve(img)
		}
		img.src = url + "?random-no-cache=" + Math.floor((1 + Math.random()) * 0x10000).toString(16)
	})
}

/**
 * Pings a url.
 * @param  {String} url
 * @param  {Number} multiplier - optional, factor to adjust the ping by.  0.3 works well for HTTP servers.
 * @return {Promise} promise that resolves to a ping (ms, float).
 */
export const ping = (url, multiplier) =>
{
	return new Promise(function (resolve, reject)
	{
		
		let start = (new Date()).getTime()
		
		request_image(url)
			.then(() =>
			{
				let delta = ((new Date()).getTime() - start)
				delta *= (multiplier || .9)
				resolve(delta)
			})
			.catch((err) =>
			{
				console.log("Image get issue:", err)
				reject(err)
			})
		
		// Set a timeout for max-pings, 5s.
		// setTimeout(function () { console.log( "Timeout on "+url) }, 10000)
	})
}

export const getPing = (url) =>
{
	let re      = new RegExp("^https?://", "i"),
	    pingUrl = re.test(url)
		    ? url
		    : "https://" + url
	
	return ping(pingUrl)
}

export const getHueCSS = (ping, attribute = "backgroundColor", forceColor = false) =>
{
	let hue = getPingHue(ping)
	
	let color      = forceColor === false
		? hue < 40 ? "white" : "black"
		: forceColor,
	    jsonString = "{\"" + attribute + "\": \"" + hue + "\", \"color\": \"" + color + "\"}"
	
	console.log(jsonString)
	
	return JSON.parse(jsonString)
}

export const getPingHue = (ping) =>
{
	let max = 300
	
	if (ping > max)
		ping = max
	
	// Green = 120, red = 0 - convert to 0-100
	
	let pc  = Math.abs(max - ping) / 4,
	    hue = pc * 1.2
	
	return "hsla(" + hue + ", 60%, 50%, 1)"
}

export const getColorIndex = (ping) =>
{
	let pings       = [50, 100, 150, 200, 250, 300],
	    bestPing    = pings.find(limit =>
	    {
		    return ping < limit
	    }) || 300,
	    returnIndex = "ping-" + bestPing
	
	// console.log("Color index of "+ping+" = "+returnIndex)
	
	return returnIndex
}

export const getIndexColorRGB = (ping) =>
{
	return indexColors[ping]
}

export const getPingAverage = (pings) =>
{
	if (pings === null || !pings.length)
		return 1000
	
	let pingList = pings.slice(-3)
	// console.log("Get average of", pings, pingList)
	
	let sum = pingList.reduce((a, b) =>
	{
		// console.log("reduce", a, b[1], "Returning:", a + b[1])
		return a + b[1]
	}, 0)
	
	return parseInt(sum / pingList.length, 10)
}

export const serversOrdered = (pinglist, servers, reverse = true) =>
{
	if (servers === undefined) console.log("No servers?", pinglist, servers)
	
	let sortServers = Object.keys(pinglist)
	                        .filter(serverName =>
	                        {
		                        // console.log("Servers in filter", servers)
		                        return servers.hasOwnProperty(serverName)
	                        })
	                        .sort((a, b) =>
	                        {
		                        let pingA = getPingAverage(pinglist[a]),
		                            pingB = getPingAverage(pinglist[b])
		
		                        return reverse
			                        ? pingA - pingB
			                        : pingB - pingA
	                        })
	
	// console.log("Ordered servers = ", servers)
	
	return sortServers
}

export const getDateString = () =>
{
	return moment.tz(moment.tz.guess()).format("YYYY-MM-DD")
}

export const getDateTimeString = () =>
{
	return moment.tz(moment.tz.guess()).format("YYYY-MM-DDTHH:mm:ss")
}

export const getCurrentLocation = () =>
{
	return new Promise((resolve, reject) =>
	{
		// Working with W3C Geolocation API
		navigator.geolocation.getCurrentPosition(
			(position) =>
			{
				console.log("You are at:", position)
				resolve(position.coords)
			},
			(err) =>
			{
				reject("Error resolving coords", err)
			},
			{
				enableHighAccuracy: true
			}
		)
	})
}

export const getArrayPings = (pings) =>
{
	return pings.map(arr =>
	{
		return arr[1]
	})
}

// We need at least 20 entries; fill rest with 0

export const pingGraphFill = (pings) =>
{
	const pingList = getArrayPings(pings)
	
	return Array(20).fill(0).concat(pingList).slice(-20)
}

export const pingServerStats = (pings, range = 20) =>
{
	if (!pings.length)
	{
		return [0, 0, 0]
	}
	
	let usePings = pings.slice((0 - range)),
	    last     = usePings.slice(-1)[0],
	    high     = [null, 0],
	    low      = last,
	    earliest = usePings[0]
	
	usePings.forEach(ping =>
	{
		if (ping[1] > high[1]) high = ping
		
		if (ping[1] < low[1]) low = ping
	})
	
	return [high, low, last, earliest]
}