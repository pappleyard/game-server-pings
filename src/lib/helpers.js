import ls from "local-storage"

import defaultState from "../defaultState"

import isURL from "validator/lib/isURL"

export const loadStateFromLocal = () =>
{
	const oldState = ls.get("GSP-" + window.gameSlug)

	return !!oldState
		? JSON.parse(oldState)
		: {}
}

export const saveStateToLocal = (data) =>
{
	ls.set("GSP-" + window.gameSlug, JSON.stringify(data))
}

export const initState = () =>
{
	let oldState = loadStateFromLocal()
	let returnState = {...defaultState, ...oldState}

	// console.log("Loaded state:", returnState)

	// Populate pings list with servers
	if (!!!returnState.pings.servers)
	{
		returnState.pings.servers = {}

		if (returnState.servers === undefined)
			returnState.servers = window.defaultServers
		
		Object.keys(returnState.servers).forEach(server =>
		{
			returnState.pings.servers[server] = []
		})
	}

	if (window.snapShot)
	{
		returnState.ui.layout = "list"
	}

	return returnState
}

export const validateIPaddress = (ipaddress) =>
{
	return /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress);

}

export const validURL = (str) =>
{
	return isURL(str)
}

export const getMissingServers = (current, original) =>
{
	let returnServers = {}

	Object.keys(original).forEach((server) =>
	{
		if (!current.hasOwnProperty(server))
		{
			returnServers = {
				...returnServers,
				[server]: original[server]
			}
		}
	})

	return Object.keys(returnServers).length
		? returnServers
		: false
}

export const getSnapshotPings = (servers) =>
{
	let recentList = {}

	for (let server in servers)
	{
		if (servers.hasOwnProperty(server) && Array.isArray(servers[server]))
		{
			recentList[server] = servers[server].slice(-20)
		}
	}

	return recentList
}