import React from "react"
import {render} from "react-dom"


import MainApp from "./containers/app-main"
import HeaderSection from "./components/header-main"

// import registerServiceWorker from './registerServiceWorker';

render(
		<HeaderSection/>,
	document.querySelector("#header-section")
)

render(
		<MainApp/>,
	document.querySelector(".app-main-container")
)
// registerServiceWorker();
