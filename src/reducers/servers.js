import {SERVER_ADD, SERVER_REMOVE, SERVER_TOGGLE, SERVERS_RESET} from "../actions/types"

import {omit} from "lodash"
import {resetServers} from "../lib/helpers";


const servers = (state = [], action) =>
{
	// console.log("Servers Action:", action)
	
	let returnState = {...state}
	
	switch (action.type)
	{
		case SERVER_TOGGLE:
			
			returnState[action.payload.server][2] = !returnState[action.payload.server][2]
			
			return returnState
		
		case SERVER_ADD:
			
			console.log("Adding server:", action, state)
			
			return {
				...state,
				[action.payload[0]]: [action.payload[2], action.payload[1], true]
			}

		case SERVER_REMOVE:

			console.log("Removing server:", action)

			return omit( state, [action.payload])

		case SERVERS_RESET:

			return action.payload
			
		default:
			return returnState
	}
}

export default servers