import {ALL_SERVERS_VIEW, SERVER_FEATURED, SERVER_INPUT_TOGGLE, SERVER_REMOVE, LAYOUT_TOGGLE, IS_SMALL} from "../actions/types"

const ui = (state = [], action) =>
{
	// console.log("UI Action:", action)
	switch (action.type)
	{
		case ALL_SERVERS_VIEW:
			
			return {
				...state,
				allServersView:
					action.payload !== "toggle"
						? action.payload
						:
						state.allServersView === "list"
							? "graph"
							: "list"
			}
		
		case SERVER_FEATURED:
			
			console.log("REDUCE featured server to", action)
			
			return {
				...state,
				featuredServer: action.payload.server
			}
		
		case SERVER_INPUT_TOGGLE:
			
			return {
				...state,
				newServerModal:
					action.payload !== "toggle"
						? action.payload
						:
						!state.newServerModal
			}
		
		case SERVER_REMOVE:
			
			return action.payload === state.featuredServer
				? {...state, "featuredServer": null}
				: state
		
		case LAYOUT_TOGGLE:
			
			console.log("Toggle layout:", action)
			
			return { ...state, layout: action.payload === state.layout ? "split" : action.payload }

		case IS_SMALL:

			// console.log("Toggle layout:", action)

			return { ...state, isSmall: action.payload }
		
		default:
			return state
	}
}

export default ui