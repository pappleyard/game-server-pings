import {combineReducers} from "redux"

import servers from "./servers"
import ui from "./ui"
import pings from "./pings"

export default combineReducers({servers, ui, pings})