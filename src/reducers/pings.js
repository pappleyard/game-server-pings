import {PING_CURRENT, RECORD_SERVER_PING, RESET_SERVER_PING, SERVER_REMOVE} from "../actions/types"
import {omit} from "lodash"

const pings = (state = [], action) =>
{
	let returnState = {...state}
	
	switch (action.type)
	{
		case RECORD_SERVER_PING:
			
			// console.log("Setting from action:", action, state)
			
			if (!returnState.servers.hasOwnProperty(action.payload.server))
			{
				returnState.servers[action.payload.server] = []
			}
			
			returnState.servers[action.payload.server].push(
				[action.payload.stamp, action.payload.ping]
			)
			
			// console.log( "Old state:", state, "New state", returnState)
			
			return returnState
		
		case PING_CURRENT:
			
			const serverName = action.payload.server,
			      setStatus  = action.payload.status,
			      foundIndex = state.current.indexOf(serverName)
			
			if (foundIndex === -1 && setStatus === true)
			{
				return {
					...state,
					current: [...state.current, serverName]
				}
			}
			else if (setStatus === false)
			{
				return {
					...state,
					current: [
						...state.current.slice(0, foundIndex),
						...state.current.slice(foundIndex + 1)
					]
				}
			}
			
			return state
		
		case SERVER_REMOVE:
			
			if (returnState.servers.hasOwnProperty(action.payload))
				returnState.servers = omit(state.servers, [action.payload])
			
			return returnState
		
		case RESET_SERVER_PING:
			
			console.log("Clearing pings for server:", action)
			
			if (state.servers.hasOwnProperty(action.payload))
			{
				returnState.servers[action.payload] = []
			}
			
			return returnState
		
		default:
			return state
	}
}

export default pings