import React, {Component} from "react"

import {Anchor, Box, Tiles, Tile, Image, Paragraph, Label} from "grommet"

import Responsive from 'grommet/utils/Responsive'

import "../styles/app.css"

export default class MainApp extends Component {
    constructor(props) {
        super(props)

        this.state = {isSmall: false}
    }

    componentDidMount() {
        this._onResponsive = Responsive.start(this._onResponsive)
    }

    _onResponsive = (small) => {
        // console.log("RESPON", small)
        this.setState({isSmall: small})
    }

    goToGame = (game) => {
        // console.log(game)
        document.location.href = "/" + Object.keys(window.gameList)[game]
    }

    render() {

        return (
            <Tiles
                selectable={true}
                onSelect={this.goToGame}
                flush={false}
                // size="large"
                fill={true} className="gametile"
                responsive={false}
            >
                <GameList isSmall={this.state.isSmall}/>
            </Tiles>
        )
    }
}

const GameList = (props) => {
    var gameSpotCount = 0

    // console.log(window.gameList)
    return Object.keys(window.gameList).map(gameSlug => {
        gameSpotCount++
        return (<React.Fragment>
                <GameBrief
                    gameSlug={gameSlug}
                    key={gameSlug}
                    gameData={window.gameList[gameSlug]}
                    isSmall={props.isSmall}
                />
            </React.Fragment>
        )
    })
}

const GameBrief = (props) => {
    const gameData = props.gameData,
        thumbSize = props.isSmall
            ? 100
            : 200,
        wordCount = gameData[1].split(" ").length,
        charLength = gameData[1].length

    let tileHeight = props.isSmall ?
        wordCount < 12
            ? 180
            : 300
        : 500

    console.log(gameData[0] + " -- ", tileHeight, gameData[1], wordCount, charLength, charLength / wordCount)

    return (<Tile>

            <Box
                align="start"
                alignContent="start"
                // basis="1/3"
                direction="row"
                justify="between"
                responsive={false}
                flex="grow"
                pad="none"
                margin="small"
                colorIndex="light-1"
                basis="full"
                style={{boxShadow: "0 1px 3px #20274830"}}
            >
                <div style={{width: thumbSize + "px", alignContent: "center"}}>
                    <Image alt={gameData[0]}
                           fit="cover"
                           src={!!gameData[3] ? window.thisDomain + "/images/games/small/" + gameData[3] : "https://via.placeholder.com/" + thumbSize}/>
                </div>

                <div style={{
                    width: (thumbSize + 100) + "px",
                    height: thumbSize + "px",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "space-between",
                    marginLeft: "15px",
                    padding: "10px 0"
                }}>
                    <Label margin="none">
                        {gameData[0]}
                    </Label>
                    <Paragraph size="medium" margin="small" className={"front-game-blurb-" + gameData[0]}>
                        <span dangerouslySetInnerHTML={{__html: gameData[1].replace(/\\(.)/mg, "$1")}}/>
                    </Paragraph>
                    <Anchor label="See pings" href={window.thisDomain + "/" + props.gameSlug}/>
                </div>

            </Box>
        </Tile>
    )
}