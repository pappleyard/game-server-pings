import React, {Component} from "react"
import {connect} from "react-redux"
import {bindActionCreators} from "redux"

import Responsive from 'grommet/utils/Responsive'

import GameInfo from "../components/gameinfo"
import ServerMeter from "../components/servermeter"
import ServerListMeter from "../components/serverlistmeter"

import {getPing} from "../lib/funcs"

import {addCustomServer, currentPingServer, recordServerPing, serverInputToggle, layoutToggle, setIsSmall, resetServers} from "../actions"

import NewServerModal from "../components/serverlist/newservermodal"

import "../styles/app.scss"
import Card from "grommet/components/Card";


class App extends Component
{
	constructor(props)
	{
		super(props)
		this.pingInterval = 5000
		this.timers = {}
		this.initialising = true
	}

	componentDidMount () {
		this._onResponsive = Responsive.start(this._onResponsive);

			var adsbygoogle
			(adsbygoogle = window.adsbygoogle || []).push({});
	}

	_onResponsive = (small) =>
	{
		this.props.setIsSmall(small)
		if (small)
		{
			this.props.layoutToggle()
		}
	}
	
	initPingLoops = () =>
	{
		const props   = this.props,
		      servers = props.servers
		
		let i = 0
		
		Object.keys(servers).forEach(serverName =>
		{
			setTimeout(() =>
			{
				// console.log("Starting .. "+ serverName)
				this.pingServerTimeout(serverName, true)
			}, i * 400)
			i++
		})
		
		setTimeout(
			() => {this.initialising = false},
			Object.keys(servers).length*400 + 1000
		)
	}
	
	pingServerTimeout = (serverName, immediate = false) =>
	{
		const props      = this.props,
		      serverData = props.servers[serverName]
		
		// console.log("Timing out " + serverName)
		
		this.timers[serverName] = setTimeout(() =>
		{
			if (props.servers.hasOwnProperty(serverName) && serverData[2] === true)
			{
				props.currentPingServer(serverName)
				
				getPing(serverData[0], serverName)
					.then((delta) =>
					{
						// console.log( `${serverName} @ ${pingServer[0]} pinged, got ${delta}`)
						props.addServerPing(serverName, delta)
						props.currentPingServer(serverName, false)
					})
					.catch((err) =>
					{
						// console.log("PING ERROR!", err)
						props.addServerPing(serverName, 5000)
						props.currentPingServer(serverName, false)
					})
			}
			
			clearTimeout(this.timers[serverName])
			this.pingServerTimeout(serverName)
			
		}, immediate
			? 0
			: this.pingInterval)
	}
	
	componentWillMount()
	{
		if (!window.snapShot)
		{
			this.initPingLoops()
		}
	}
	
	componentWillUnmount()
	{
		// clearInterval(this.interval)
	}
	
	componentWillUpdate()
	{
		if (!this.initialising)
		{
			const props   = this.props,
			      servers = props.servers
			
			Object.keys(servers)
			      .filter(serverName =>
			      {
				      return !this.timers.hasOwnProperty(serverName)
			      })
			      .forEach(serverName =>
			      {
				      this.pingServerTimeout(serverName, true)
			      })
		}
	}
	
	render()
	{
		const props = this.props
		
		// console.log("Render main app:", this.props)
		return (
			<React.Fragment>
				{props.ui.newServerModal
					? <NewServerModal
						serverInputToggle={props.serverInputToggle}
						addCustomServer={props.addCustomServer}
						servers={props.servers}
					/>
					: null}
				<GameInfo isSmall={props.ui.isSmall}/>
				<ServerMeter/>
				<ServerListMeter/>
			</React.Fragment>
		)
	}
}

const
	mapDispatchToProps = dispatch => bindActionCreators({
		addServerPing: recordServerPing,
		currentPingServer,
		serverInputToggle,
		addCustomServer,
		layoutToggle,
		setIsSmall,
		resetServers
	}, dispatch)

const
	mapStateToProps = state => ({
		servers: state.servers,
		pings: state.pings,
		ui: state.ui
	})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(App)