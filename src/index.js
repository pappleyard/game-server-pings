import React from "react"
import {render} from "react-dom"

import {createStore} from "redux"
import {Provider} from "react-redux"
import rootReducer from "./reducers"

import App from "./containers/app-built"
import HeaderSection from "./components/header"

import {initState, saveStateToLocal} from "./lib/helpers"

let initialState = initState()

const store = createStore(
	rootReducer,
	initialState,
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

window.pingtimes = {}

store.subscribe(() =>
{
	saveStateToLocal(store.getState())
})

// import registerServiceWorker from './registerServiceWorker';

render(
	<Provider store={store}>
		<HeaderSection/>
	</Provider>,
	document.querySelector("#header-section")
)

render(
	<Provider store={store}>
		<App/>
	</Provider>,
	document.querySelector(".app-main-container")
)
// registerServiceWorker();
