import React from "react"
import {connect} from "react-redux"
import {bindActionCreators} from "redux"
import PropTypes from "prop-types"

import {Anchor, Box, Label, Title} from "grommet"
import AddCircle from "grommet/components/icons/base/AddCircle"
import Revert from "grommet/components/icons/base/Revert"
// import Tasks from "grommet/components/icons/base/Tasks"
// import FingerPrint from "grommet/components/icons/base/FingerPrint"
import Expand from "grommet/components/icons/base/Expand"
import Contract from "grommet/components/icons/base/Contract"

import {Timeline} from "react-twitter-widgets"

import {
	deleteServer,
	layoutToggle,
	serverInputToggle,
	serverToggle,
	setFeaturedServer,
	toggleListView,
	resetServers
} from "../actions"

import {ListFormat, MeterFormat} from "./serverlist"


const ServerListMeter = props => {
	const ui = props.ui,
		servers = props.servers,
		pings = props.pings

	// console.log("ServerListMeter:", servers, pings, ui)

	return (
		<Box
			colorIndex="light-1"
			basis={ui.layout === "split"
				? "1/3"
				:
				ui.layout === "list"
					? "2/3"
					: "small"}
			align="center"
			justify="start"
			className="server-list-box"
			flex={false}
		>
			<Box
				justify="between"
				align="center"
				pad={{horizontal: "none", vertical: "none"}}
				size="medium"
				direction="column"
				style={{width: "95%"}}
				responsive={false}
			>
				<Box
					pad={{vertical: "medium"}}
					basis="full"
				/>
				<Title style={{lineHeight: "2"}} basis="full">All Servers</Title>

				<Box
					direction="row"
					pad={{between: "medium"}}
					justify="center"
					responsive={false}
				>
					{!!!window.snapShot ? <React.Fragment>
							<Box
								margin={{vertical: "small"}}
								align="center"
								justify="start"
								direction="column"
								responsive={false}
							>
								<Anchor
									icon={<AddCircle size="small"/>}
									onClick={() => props.serverInputToggle(true)}
								/>
								<Label
									style={{margin: "-13px 0 0 0"}}
									size="small"
								>
									add
								</Label>
							</Box>

						{(JSON.stringify(servers) !== JSON.stringify(window.defaultServers) ?
							<Box
								margin={{vertical: "small"}}
								align="center"
								justify="start"
								direction="column"
								responsive={false}
							>
								<Anchor
									icon={<Revert size="small"/>}
									onClick={() => props.resetServers()}
								/>
								<Label
									style={{margin: "-13px 0 0 0"}}
									size="small"
								>
									default list
								</Label>
							</Box>
							: null)}
					</React.Fragment>
						: null}

					<Box
						margin={{vertical: "small"}}
						align="center"
						justify="start"
						direction="column"
						responsive={false}
					>
						<Anchor
							icon={ui.layout === "list"
								? <Contract size="small"/>
								: <Expand size="small"/>}
							onClick={() => props.layoutToggle("list")}
						/>
						<Label
							style={{margin: "-13px 0 0 0"}}
							size="small">
							{ui.layout === "list"
								? "show meter"
								: "hide meter"}
						</Label>
					</Box>
				</Box>
			</Box>

			<Box pad={{vertical: "small"}}/>
			{ui.allServersView === "graph" ? (
				<MeterFormat
					servers={servers}
					pings={pings}
					ui={ui}
					setFeaturedServer={props.setFeaturedServer}
				/>
			) : (
				<ListFormat
					servers={servers}
					ui={ui}
					pings={pings}
					setFeaturedServer={props.setFeaturedServer}
					deleteServer={props.deleteServer}
					serverToggle={props.serverToggle}
					layoutToggle={props.layoutToggle}
				/>
			)}

			{window.gameTwitter
				? <p><a href={"https://twitter.com/" + window.gameTwitter}>Twitter page for {window.gameTwitterName || window.gameName}</a></p>
				: null}
		</Box>
	)
}

ServerListMeter.propTypes = {
	servers: PropTypes.object.isRequired,
	pings: PropTypes.object.isRequired,
	ui: PropTypes.object.isRequired
}

const mapStateToProps = state => {
	return {
		servers: state.servers,
		pings: state.pings,
		ui: state.ui
	}
}

const mapDispatchToProps = dispatch => bindActionCreators({
	toggleListView,
	setFeaturedServer,
	serverInputToggle,
	deleteServer,
	serverToggle,
	layoutToggle,
	resetServers
}, dispatch)

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ServerListMeter)
