import React from "react"
import PropTypes from "prop-types"
import {Meter, Value} from "grommet"
import {getColorIndex, getPingAverage, pingServerStats, serversOrdered} from "../../lib/funcs"

const MeterFormat = props =>
{
	let servers     = props.servers,
	    pings       = props.pings.servers,
	    ui          = props.ui,
	
	    meterSeries = serversOrdered(pings, servers).map(serverName =>
	    {
		    let colorIndex = servers[serverName][2] // Grey if disabled
			    ? getColorIndex(getPingAverage(pings[serverName]))
			    : "light-1",
		        ping       = pingServerStats(pings[serverName])[2]
		
		    return {
			    label: ui.featuredServer === serverName ? <Value size="small">{serverName}</Value> : serverName,
			    value: ping[1] || 0,
			    colorIndex: colorIndex,
			    onClick: () => props.setFeaturedServer(serverName)
		    }
	    })
	
	console.log("Meter series:", meterSeries)
	
	return (
		<Meter
			type="spiral"
			className="server-list-spiral"
			max={1000}
			series={meterSeries}
		/>
	)
}

MeterFormat.propTypes = {
	servers: PropTypes.object.isRequired,
	pings: PropTypes.object.isRequired
}

export default MeterFormat