import React, {Component} from "react"
import PropTypes from "prop-types"
import {Anchor, Area, Axis, Base, Box, Button, Chart, Grid, Headline, Label, Layer, Layers, List, ListItem, Value} from "grommet"

import Pause from "grommet/components/icons/base/Pause"
import Play from "grommet/components/icons/base/Play"
import Trash from "grommet/components/icons/base/Trash"

import {getColorIndex, getIndexColorRGB, pingGraphFill, pingServerStats, serversOrdered} from "../../lib/funcs"


const ExtraList = (props) =>
{
	const serverName = props.serverName,
	      pings      = props.pings.servers
	
	let serverPings       = pings[serverName],
	    [high, low, last] = pingServerStats(serverPings, 20),
	    pinglistHistory   = pingGraphFill(serverPings)
	
	return (
		[
			<Box
				direction="row"
				justify="between"
				align="center"
				pad={{between: "small"}}
				responsive={false}
				key="latencies"
			>
				<Label size="small">
					high:&nbsp;{high[1]}ms
					low: {low[1]}ms
				</Label>
			</Box>,
			
			<Box
				pad="none"
				margin="none"
				key="listgraph"
			>
				<Chart full={false} vertical={true}>
					{/*<Axis count={3} vertical={true} labels={[{index: 2, label: high[1]}]}/>*/}
					<Base height="xxsmall" width="small"/>
					<Layers>
						<Area
							values={pinglistHistory}
							colorIndex="graph-1"
							activeIndex={pinglistHistory.length + 1}
							max={high[1]}
						/>
						<Grid rows={3} columns={20}/>
					</Layers>
				</Chart>
			</Box>
		]
	)
}

class ListFormat extends Component
{
	constructor(props)
	{
		super(props)
		
		this.state = {
			confirmDelete: null
		}
	}
	
	confirmDelete = () =>
	{
		return (
			<Layer
				onClose={() => this.setState({confirmDelete: null})}
				overlayClose={true}
				closer={true}
			>
				<Box size="medium" pad="large">
					<Headline
						size="small"
						string={true}
					>
						Delete server<br/>'{this.state.confirmDelete}'?
					</Headline>
					
					<Button
						icon={<Trash/>}
						label="Delete"
						critical={true}
						onClick={() =>
						{
							this.setState({confirmDelete: null})
							this.props.deleteServer(this.state.confirmDelete)
						}}
						fill={false}
					/>
				
				</Box>
			</Layer>
		)
	}
	
	render()
	{
		let props        = this.props,
		    state        = this.state,
		    servers      = props.servers,
		    pings        = props.pings.servers,
		    orderedPings = serversOrdered(pings, servers),
		    i            = 0,
		    hideMeter = props.ui.layout === "list"
		
		// console.log( "servers pings", servers, pings)
		
		return (
			<React.Fragment>
				{state.confirmDelete
					?
					this.confirmDelete()
					: null}
				<List selectable={true}
				      selected={[
					      orderedPings.findIndex((el) =>
					      {
					      	if (el === undefined || props.ui === undefined || props.ui.featuredServer === undefined)
							{
								return -1
							}
						      return props.ui.featuredServer === el
					      })
				      ]}
				      full="horizontal"
				      style={{width: "95%", marginLeft: "0"}}
				>
					{orderedPings.map(serverName =>
					{
						if (servers.hasOwnProperty(serverName))
						{
							let pingServer    = pings[serverName],
							    lastPing      = pingServer.slice(-1)[0],
							    isEnabled     = servers[serverName][2],
							    currentServer = props.pings.current.indexOf(serverName) !== -1
							
							i++
							
							// console.log("Ping Server "+serverName, pingServer, "Last", lastPing)
							
							let ping = pingServer.length === 0 ? null : lastPing[1] // Most recent ping
							
							return (
								<ListItem
									justify={hideMeter ? "center" : "between"}
									pad={{between: hideMeter ? "medium" : "small", vertical: "none"}}
									separator={i === 1
										? "horizontal"
										: "bottom"}
									className="ping-list-row"
									key={serverName}
									onClick={() =>
									{
										props.setFeaturedServer(serverName)
										props.layoutToggle("split")
									}}
									responsive={hideMeter}
								>
									{!!!window.snapShot ? <div className="control-box">
										<Anchor
											icon={servers.hasOwnProperty(serverName) && servers[serverName][2] === true
												? <Pause size="xsmall" style={{height: "70%"}}/>
												: <Play size="xsmall" style={{height: "70%"}}/>}
											onClick={() =>
											{
												props.serverToggle(serverName)
											}}
										/>
										
										<Anchor
											icon={<Trash size="xsmall" style={{height: "70%"}}/>}
											onClick={() =>
											{
												this.setState({confirmDelete: serverName})
											}}
										/>
									</div> : null}
									<Box
										direction="row"
										justify="between"
										align="center"
										pad={{horizontal: "small", vertical: "none"}}
										// responsive={false}
										basis="full"
									>
										<Label
											size="medium"
											margin="small"
											className={"server-list-name" + (currentServer ? " pinging-server" : "")}
											style={{position: "relative"}}
											align={hideMeter ? "center" : "start"}
										>
											{serverName}
											
											{servers[serverName][1]
												? (
													<Label
														size="small"
														margin="none"
														className="server-list-sub"
													>
														<br/>
														{servers[serverName][1]}
													</Label>
												)
												: null}
										</Label>
									</Box>
									
									<Box
										direction="row"
										justify="center"
										align="center"
										pad={{horizontal: "small", vertical: "none"}}
										// responsive={false}
									>
										<Value
											value={ping ? ping : "-"}
											size="small"
											className="ping-list-value"
											style={{
												borderBottomColor: isEnabled && ping
													? getIndexColorRGB(getColorIndex(ping))
													: "rgba( 0, 0, 0, 0.3)",
												color: isEnabled
													? "black"
													: "rgba( 0, 0, 0, 0.3)"
											}}
										/>
									</Box>
									{props.ui.layout !== "list"
										? null
										:
										<ExtraList
											{...props}
											serverName={serverName}
										/>}
								</ListItem>
							)
						}
						else
							return null
					})}
				</List>
			</React.Fragment>
		)
	}
}

ListFormat.propTypes = {
	servers: PropTypes.object.isRequired,
	pings: PropTypes.object.isRequired
}

export default ListFormat