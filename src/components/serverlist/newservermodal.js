import React, {Component} from "react"
import PropTypes from "prop-types"
import {Box, Button, Footer, Form, FormField, FormFields, Headline, Layer, Paragraph, Select, TextInput, Title} from "grommet"
import Spinning from "grommet/components/icons/Spinning"

import {getMissingServers, validateIPaddress, validURL} from "../../lib/helpers"
import {getPing} from "../../lib/funcs"

import {serverList} from "../gamesdata/pubgservers"

class NewServerModal extends Component
{
	constructor(props)
	{
		super(props)
		
		this.state = {
			serverName: "",
			serverSub: "",
			serverAddress: "",
			addServer: "",
			errors: {"serverName": "", "serverSub": "", "serverAddress": ""},
			missingServers: getMissingServers(props.servers, serverList),
			checkingServer: false
		}
		
		this.validFormFields = ["serverName", "serverSub", "serverAddress"]
		
		// console.log("Missing servers = ", this.state.missingServers)
	}
	
	handleChange = (ev) =>
	{
		const state  = this.state,
		      name   = ev.target.name,
		      value  = ev.target.value,
		      errors = state.errors
		
		// console.log("Handle change = ", ev)
		
		if (name === "addServer")
		{
			const altValue  = ev.option,
			      oldServer = state.missingServers[altValue]
			
			
			// console.log(`Retrieving data for ${altValue}: `, oldServer)
			
			this.setState({
				"addServer": altValue,
				"serverName": altValue,
				"serverSub": oldServer[1],
				"serverAddress": oldServer[0]
			})
			
			return
		}
		
		// Revalidate if errors hanging over ..
		if (errors[name].length)
		{
			this.validate(name, value)
		}
		
		// console.log("Change:", name, value)
		this.setState({[name]: value})
	}
	
	handleSubmit = () =>
	{
		console.log("Submitting", this.state)
		const state = this.state
		
		if (!state.addServer)
		{
			let hasErrors = false
			
			this.validFormFields.forEach(field =>
			{
				if (this.validate(field) !== "")
				{
					hasErrors = true
				}
			})
			
			if (hasErrors)
				return false
		}
		
		this.setState({checkingServer: true})
		
		getPing(state.serverAddress, state.serverName)
			.then((delta) =>
			{
				this.props.addCustomServer(state.serverName, state.serverSub, state.serverAddress)
				this.props.serverInputToggle(false)
			})
			.catch( (err) => {
				console.log( "PING ERROR!", err)
				this.setState( { errors: { serverAddress: "Test ping timed out"}, checkingServer: false })
			})
	}
	
	validate = (field, value = null) =>
	{
		const state  = {...this.state},
		      errors = state.errors
		
		if (value === null)
			value = state[field]
		
		console.log("Validating " + field + " for", value)
		
		switch (field)
		{
			case "serverName":
				
				errors.serverName = value.length > 0
					? ""
					: "Server name is required"
				
				break
			
			case "serverAddress":
				
				errors.serverAddress = ""
				
				if (!validateIPaddress(value) && !validURL(value))
				{
					errors.serverAddress += "Address is not a valid domain or IPv4."
				}
				
				break
			
			default:
				break
		}
		
		this.setState({errors: errors})
		
		return errors[field]
	}
	
	render()
	{
		const props  = this.props,
		      state  = this.state,
		      errors = state.errors
		
		return (
			<Layer closer={true}
			       flush={false}
			       overlayClose={true}
			       onClose={() => props.serverInputToggle(false)}
			>
				{state.checkingServer
				? 	<Box
						colorIndex="grey-4"
						size="large"
						align="center"
						justify="start"
						margin="medium"
					>
						<Box pad={{vertical: "medium"}}/>
						<Title align="center">Pinging new server</Title>
						<Box pad={{vertical: "large"}}/>
						<Spinning align="center" size="large"/>
					</Box>
				     :
					<React.Fragment>
						<Form pad="medium">
						<Headline size="small">Add New Server</Headline>
						<Paragraph size="small">Enter details for the new server then click 'Add'</Paragraph>
						<FormFields>
							<FormField
								label="Name of new server (required)"
								error={errors.hasOwnProperty("serverName")
									? errors.serverName
									: null}
							>
								<TextInput
									name="serverName"
									onDOMChange={this.handleChange}
									value={state.serverName}
									onBlur={() => this.validate("serverName")}
								/>
							</FormField>
							
							<FormField
								label="Optional subtitle"
							>
								<TextInput
									name="serverSub"
									onDOMChange={this.handleChange}
									value={state.serverSub}
								/>
							</FormField>
							
							<FormField
								label="Server address or IP (v4)"
								error={errors.hasOwnProperty("serverAddress")
									? errors.serverAddress
									: null}
							>
								<TextInput
									name="serverAddress"
									onDOMChange={this.handleChange}
									value={state.serverAddress}
									onBlur={() => this.validate("serverAddress")}
								/>
							</FormField>
							{state.missingServers
								?
								<React.Fragment>
									<Paragraph>
										Or,populate with an existing server:
									</Paragraph>
									<Select
										options={Object.keys(state.missingServers)}
										onChange={this.handleChange}
										name="addServer"
										value={state.addServer}
									/>
								</React.Fragment>
								: null
							}
						</FormFields>
					</Form>
						<Footer
							pad={{"vertical": "medium"}}
							justify="center"
						>
							<Button label='Add Server'
							        onClick={this.handleSubmit}
							        primary={true}
							        secondary={false}/>
						</Footer>
					</React.Fragment>}
			</Layer>
		)
	}
}

NewServerModal.propTypes = {
	serverInputToggle: PropTypes.func.isRequired
}

export default NewServerModal