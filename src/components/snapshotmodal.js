import React from "react"
import PropTypes from "prop-types"
import {Button, Footer, Form, FormField, FormFields, Layer, Paragraph, TextInput} from "grommet"
import moment from "moment-timezone"

const SnapShotModal = (props) =>
{
	return (
		<Layer closer={true}
		       flush={false}
		       overlayClose={true}
		       onClose={props.cancelSnapshot}
		>
			<Form pad="medium" method="post" action="/makesnapshot">
				<input type="hidden" name="timezone" value={moment.tz.guess()}/>
				<input type="hidden" name="_token" value={window.csrftoken}/>
				<input type="hidden" name="game" value={window.gameSlug}/>
				<input type="hidden" name="statedata" value={JSON.stringify(props.stateData)}/>
				
				<Paragraph size="medium">Enter a brief tagline (100 characters or less) for your snapshot then hit 'Submit'.</Paragraph>
				<FormFields>
					<FormField
						// label="Snapshot Description"
					>
						<TextInput
							name="description"
							id="snapshot-description"
							maxLength={100}
							required
						/>
					</FormField>
				</FormFields>
				<Footer
					pad={{"vertical": "medium"}}
					justify="center"
				>
					<Button label='Submit'
					        primary={true}
					        type="submit"
					/>
					<Button label='Cancel'
					        secondary={true}
					        onClick={props.cancelSnapshot}
					/>
				</Footer>
			</Form>
		</Layer>
	)
}

SnapShotModal.propTypes = {
	stateData: PropTypes.object.isRequired,
	cancelSnapshot: PropTypes.func.isRequired
}

export default SnapShotModal