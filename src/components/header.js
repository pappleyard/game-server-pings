import React from "react"
import {connect} from "react-redux"

import moment from "moment-timezone"

import {Anchor, Box, Button, Header, Heading, Label, Menu, Title, Toast} from "grommet"

import Camera from "grommet/components/icons/base/Camera"

import {getSnapshotPings} from "../lib/helpers"
import pingsLogo from "../resources/images/gameserverping-logo-square-border.png"
import GspLogoText from "./gsptext"

import SnapShotModal from "./snapshotmodal"

class HeaderSection extends React.Component
{
	
	constructor(props)
	{
		super(props)
		
		this.state = {
			doSnapshot: false
		}
	}
	
	cancelSnapshot = () =>
	{
		this.setState({doSnapshot: false})
	}
	
	errorToast()
	{
		return (
			<Toast status='critical'
			       onClose={() => window.postErrors = null}>
				<h3>Error</h3>
				<ul>
					<li>{window.postErrors.join("</li><li>")}</li>
				</ul>
			</Toast>
		)
	}
	
	render()
	{
		const props = this.props,
		      state = this.state
		
		return (
			<div style={{width: "100%"}}>
				
				{window.postErrors
					? this.errorToast()
					: null}
				
				{state.doSnapshot
					? <SnapShotModal
						stateData={{servers: props.servers, pings: props.pings}}
						cancelSnapshot={this.cancelSnapshot}
					/>
					: null}
				
				<Header justify="center"
				        colorIndex="neutral-4"
				        size="medium"
						style={{backgroundColor: window.barColor}}
				>
					
					<Box direction="row"
					     responsive={false}
					     justify="between"
					     align="center"
					     flex={true}
					     style={{maxWidth: "1152px"}}
					>
						
						<a href="/"><Title className="main-title"><img src={pingsLogo} alt="game server ping"/><Box pad={{horizontal: "small"}}/><GspLogoText/></Title></a>
						
						{window.snapShot
							? null
							: <Button
									icon={<Camera/>}
									primary={true}
									label={props.ui.isSmall ? null : "Share Snapshot"}
									responsive="true"
									onClick={() => this.setState({doSnapshot: true})}
									size="small"
								/>}
						
						<Box align="end">
							<Menu
								responsive={true}
								direction="row"
								inline={false}
								label={props.ui.isSmall ? "Other games" : "See Your Pings For"}
								align="end"
								justify="end"
								className="games-menu"
								dropAlign={{right: "right", top: "top"}}
								style={{backgroundColor: "rgba(255, 255, 255, 0.6)"}}
							>{Object.keys(window.gameList).map(gameSlug =>
							{
								return <Anchor
									key={gameSlug}
									href={"/" + gameSlug}
									allyTitle={window.gameList[gameSlug][4]}
									title={window.gameList[gameSlug][4]}
								>{window.gameList[gameSlug][0]}</Anchor>
							})}
							</Menu>
						</Box>
					</Box>
				
				</Header>
				{window.snapShot
					? <Box
					size="large"
					direction="column"
					colorIndex="accent-1"
					full="horizontal"
					justify="center"
					pad="medium"
					textAlign="center"
					style={{width: "100%"}}
					>
						<Heading tag="h3">
							Server Ping Snapshot - {window.snapShot}
						</Heading>
						<Label size="small">Taken {moment.utc(window.snapShotStamp).local().format("LLL")} (your time)</Label>
					</Box>
					: null}
			</div>
		)
	}
}

const mapStateToProps = state => ({
	servers: state.servers,
	pings: getSnapshotPings(state.pings.servers),
	ui: state.ui
})

export default connect(
	mapStateToProps,
	null
)(HeaderSection)