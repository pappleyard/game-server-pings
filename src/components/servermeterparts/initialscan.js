import React from "react"
import {Box, Title} from "grommet"
import Spinning from "grommet/components/icons/Spinning"

const InitialScan = () => (
	<Box
		colorIndex="grey-4"
		basis="1/3"
		size="large"
		align="center"
		justify="start"
	>
		<Box pad={{vertical: "medium"}}/>
		<Title align="center">Running initial ping scan</Title>
		<Box pad={{vertical: "large"}}/>
		<Spinning align="center" size="large"/>
	</Box>
)

export default InitialScan