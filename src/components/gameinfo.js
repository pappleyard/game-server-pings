import React from "react"

import Card from "grommet/components/Card"

const gameDesc = <h4 dangerouslySetInnerHTML={{__html: window.gameDesc}}></h4>
const gameBlurb = <span dangerouslySetInnerHTML={{__html: window.gameBlurb}}></span>

const mainName = window.gameName.match(/[^(]+/)
const subName = window.gameName.match(/\([^)]+\)/)
const headingContent = <h2 className="grommetux-heading">{mainName}{subName != null ? <br/> : null}{subName != null ? <span style={{fontSize: "80%"}}>{subName}</span> : null}</h2>

const GameInfo = (props) => (
	
	<Card
		heading={headingContent}
		description={gameDesc}
		headingStrong={false}
		basis="1/3"
		flex={false}
		colorIndex="grey-2-a"
		thumbnail={window.gameImage}
		className="game-info-card"
		style={window.gameColor ? {backgroundColor: window.gameColor} : {}}
	>
		<br/>

		<ins className="adsbygoogle"
			 style={{display: "block"}}
			 data-ad-client="ca-pub-9362258405655236"
			 data-ad-slot="8812419350"
			 data-ad-format="auto"
			 data-full-width-responsive="true"></ins>
		<br/>
		{props.isSmall ? null : gameBlurb}

	</Card>

)

export default GameInfo