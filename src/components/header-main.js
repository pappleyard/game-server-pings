import React from "react"

import {Anchor, Box, Header, Menu, Title, Toast} from "grommet"

import Responsive from 'grommet/utils/Responsive'

import pingsLogo from "../resources/images/gameserverping-logo-square-border.png"
import GspLogoText from "./gsptext"

export default class HeaderSection extends React.Component
{
    constructor(props)
	{
		super(props)

		this.state = { isSmall: false}
	}

	componentDidMount () {
		this._onResponsive = Responsive.start(this._onResponsive);
	}

	_onResponsive = (small) =>
	{
		this.setState( { isSmall: small })
	}

	render()
	{
		return (
			<div style={{width: "100%"}}>

				<Header justify="center"
				        colorIndex="neutral-4"
				        size="medium"
						style={{backgroundColor: window.barColor}}
				>

					<Box direction="row"
					     responsive={false}
					     justify="between"
					     align="center"
					     flex={true}
					     style={{maxWidth: "1152px"}}
					>

						<a href="/"><Title className="main-title"><img src={pingsLogo} alt="game server ping"/><Box pad={{horizontal: "small"}}/><GspLogoText/></Title></a>

						<Box align="end">
							<Menu
								responsive={true}
								direction="row"
								inline={false}
								label={this.state.isSmall ? "Other games" : "See Your Pings For"}
								align="end"
								justify="end"
								className="games-menu"
								dropAlign={{right: "right", top: "top"}}
								style={{backgroundColor: "rgba(255, 255, 255, 0.6)"}}
							>{Object.keys(window.gameList).map(gameSlug =>
							{
								return <Anchor
									key={gameSlug}
									href={"/" + gameSlug}
									allyTitle={window.gameList[gameSlug][4]}
									title={window.gameList[gameSlug][4]}
								>{window.gameList[gameSlug][0]}</Anchor>
							})}
							</Menu>
						</Box>
					</Box>

				</Header>
			</div>
		)
	}
}