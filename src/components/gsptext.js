import React from "react"

import {Box} from "grommet"

const GspLogoText = (size = "100%") =>
{
	return <Box
		className="gsp-logotext"
		style={{fontSize: size}}
		direction="row"
		align="center"
	>
		<div>Game Server</div> <span>PING</span></Box>
}

export default GspLogoText