const serverList =
	      {
		      Oceania: [
			      "dynamodb.ap-southeast-2.amazonaws.com",
			      "AWS Sydney",
			      true
		      ],
		      "North America": ["dynamodb.us-east-2.amazonaws.com", "AWS East", true],
		
		      Europe: ["185.82.209.9", "servers.com", true],
		
		      "Asia 2": ["dynamodb.ap-northeast-2.amazonaws.com", "AWS S. Korea", true],
		      "Asia 1": ["dynamodb.ap-northeast-1.amazonaws.com", "AWS Japan", true],
		
		      "South-East Asia": ["dynamodb.ap-southeast-1.amazonaws.com", "AWS Singapore", true],
		
		      "South America": ["dynamodb.sa-east-1.amazonaws.com", "AWS Brazil", true],
		
		      China: ["dynamodb.cn-north-1.amazonaws.com.cn", "AWS Beijing", true]
	      }
	      
export {serverList}