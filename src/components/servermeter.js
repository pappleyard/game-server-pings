import React, {Component} from "react"
import ReactDOM from "react-dom"
import connect from "react-redux/es/connect/connect"
import PropTypes from "prop-types"

import moment from "moment-timezone"

import {Anchor, Animate, Area, Axis, Base, Box, Button, Chart, Grid, Headline, HotSpots, Label, Layer, Layers, MarkerLabel, Meter, Title, Value} from "grommet"

import {getColorIndex, pingGraphFill, pingServerStats, serversOrdered} from "../lib/funcs"
import {deleteServer, resetServerPing, serverToggle} from "../actions"

import InitialScan from "./servermeterparts/initialscan"
import {bindActionCreators} from "redux"
import Pause from "grommet/components/icons/base/Pause"
import Play from "grommet/components/icons/base/Play"
import Trash from "grommet/components/icons/base/Trash"
import Clear from "grommet/components/icons/base/Clear"
import Card from "grommet/components/Card"

const getFeaturedServer = (pings, ui, servers) =>
{
	return ui.featuredServer !== null
		?
		ui.featuredServer
		:
		Object.keys(pings).filter(pingServer =>
		{
			return pings[pingServer].length < 1
		}).length > 0
			? "placer"
			: serversOrdered(pings, servers)[0] // Best ping server
}

const FadeAnimate = (props) =>
{
	return (
		<Animate
			enter={{"animation": "fade", "duration": 300, "delay": 0}}
			leave={{"animation": "fade", "duration": 300, "delay": 0}}
			keep={true}
		>
			<Box
				direction={props.direction}
				justify={props.justify}
				align={props.align}
			>{props.children}</Box>
		</Animate>
	)
}

FadeAnimate.defaultProps =
	{
		direction: "row",
		justify: "start",
		align: "center"
	}

class ServerMeter extends Component
{
	constructor(props)
	{
		super(props)
		
		this.state = {
			confirmClear: null,
			graphActive: null
		}
	}
	
	confirmClear = () =>
	{
		return ReactDOM.createPortal(
			<Layer
				onClose={() => this.setState({confirmClear: null})}
				overlayClose={true}
				closer={true}
			>
				<Box size="medium" pad="large">
					<Headline
						size="small"
					>
						Clear ping history for <br/>'{this.state.confirmClear}'?
					</Headline>
					
					<Button
						icon={<Clear/>}
						label="Clear"
						critical={true}
						onClick={() =>
						{
							this.setState({confirmClear: null})
							this.props.resetServerPing(this.state.confirmClear)
						}}
						fill={false}
					/>
				
				</Box>
			</Layer>,
			document.querySelector("#modalRoot")
		)
	}
	
	render()
	{
		const state       = this.state,
		      props       = this.props,
		      pings       = props.pings.servers,
		      ui          = props.ui,
		      servers     = props.servers,
		      graphActive = state.graphActive
		
		if (ui.layout === "list")
		{
			return <div></div>
		}
		
		// console.log("ServerMeter Props", props)
		
		const showFeaturedServer = getFeaturedServer(pings, ui, servers)
		
		// console.log("Featured server:", showFeaturedServer)
		
		if (showFeaturedServer === "placer")
		{
			return <InitialScan/>
		}
		
		let serverPings                 = pings[showFeaturedServer],
		    pingsTwenty                 = serverPings.slice(-20),
		    serverPingsCount            = serverPings.length,
		    graphPingCount              = serverPingsCount < 20 ? serverPingsCount : 20,
		    [high, low, last, earliest] = pingServerStats(serverPings, 20),
		    pinglistHistory             = pingGraphFill(serverPings)
		
		let activePing = !!graphActive
			? graphPingCount >= 20
				? graphActive
				: graphActive - (20 - graphPingCount)
			: null
		
		// console.log("Earliest = "+earliest)
		
		// console.log("Ping server history", high,low, last)
		return (<Box
				colorIndex="grey-4"
				basis="1/3"
				size="large"
				align="center"
				justify="start"
				className="server-meter-box"
				flex={false}
			>
				{/*			<Animate enter={{"animation": "slide-right", "duration": 300, "delay": 0}}
			         keep={true}>
				<Box
					align="center"
					justify="start"
				pad="none"
				margin="none">*/}
				<Box pad={{vertical: "medium"}}/>
				<FadeAnimate
					direction="column"
				>
					<Label size="small" align="center">
						{showFeaturedServer !== ui.featuredServer ? "Best ping to" : "Stats for"}
					</Label>
					<Title>{showFeaturedServer}</Title>
					<Label size="small" align="center">
						{servers.hasOwnProperty(showFeaturedServer) && servers[showFeaturedServer][1] !== ""
							?
							servers[showFeaturedServer][1]
							:
							" "}
					</Label>
					
					<Box
						margin="none"
						align="center"
						justify="center"
						direction="row"
						pad="none"
						style={{width: "100%"}}
						responsive={false}
					>
						<Box
							direction="column"
							margin="medium"
							align="center"
							pad="none"
						>
							<Anchor icon={servers.hasOwnProperty(showFeaturedServer) && servers[showFeaturedServer][2] === true
								? <Pause size="small" style={{height: "70%"}}/>
								: <Play size="small" style={{height: "70%"}}/>}
							        onClick={() => props.serverToggle(showFeaturedServer)}/>
							<Label style={{margin: "-13px 0 0 0"}} size="small">
								{servers.hasOwnProperty(showFeaturedServer) && servers[showFeaturedServer][2] === true
									? "pause"
									: "unpause"}
							</Label>
						</Box>
						
						<Box
							direction="column"
							margin="medium"
							pad="none"
						>
							<Anchor icon={<Trash size="small" style={{height: "70%"}}/>}
							        onClick={() => props.deleteServer(showFeaturedServer)}/>
							<Label style={{margin: "-13px 0 0 0"}} size="small">
								remove
							</Label>
						</Box>
						
						<Box
							direction="column"
							margin="medium"
							pad="none"
							align="center"
						>
							<Anchor icon={<Clear size="small" style={{height: "70%"}}/>}
							        onClick={() =>
							        {
								        this.setState({confirmClear: showFeaturedServer})
							        }}/>
							<Label style={{margin: "-13px 0 0 0"}} size="small">
								reset
							</Label>
						</Box>
					</Box>
					
					
					<Meter
						type="arc"
						value={last[1]}
						max={500}
						colorIndex={getColorIndex(last[1])}
						size="small"
						className="server-meter-slice"
					/>
					
					<Box
						direction="row"
						justify="between"
						align="center"
						pad={{between: "small"}}
						responsive={false}
					>
						<Label size="small">0 ms</Label>
						<Value value={last[1]} units="ms"/>
						<Label size="small">500 ms</Label>
					</Box>
					
					<Box pad={{vertical: "small"}}/>
					
					<Chart
						className="server-meter-chart"
					>
						{/*<Axis
							count={6}
							vertical={true}
							labels={[{index: 5, label: high[1] + "ms"}]}
						/>*/}

						<Chart
							full={false}
							vertical={true}
						>
							<MarkerLabel count={20}
							             index={15}
							             label={!!!graphActive ? "" : pingsTwenty[activePing][1] + "ms at " + moment(pingsTwenty[activePing][0]).format("HH:mm:ss")}
							/>
							<Label align="center" size="small">High (last 90 seconds) <b>{high[1]}ms</b></Label>
							<Base height="xsmall" width="small"/>

							<Layers>
								<Area
									values={pinglistHistory}
									colorIndex="graph-1"
									activeIndex={!!!graphActive ? null : graphActive}
									max={high[1]}
								/>
								<Grid rows={6} columns={20}/>
								<HotSpots count={20}
								          max={20}
								          activeIndex={!!graphActive ? null : graphActive}
								          onActive={(el) =>
								          {
									          this.setState({
										          graphActive: el !== undefined
											          ?
											          graphPingCount > 19
												          ? el
												          :
												          el < (20 - graphPingCount)
													          ? (20 - graphPingCount)
													          : el
											          : el
									          })
								          }}
								/>
							</Layers>
							<Axis count={20}
							      labels={[
								      {
									      "index": serverPingsCount >= 20 || serverPingsCount < 10
										      ? 0
										      : (20 - serverPingsCount),
									      "label": graphPingCount > 10
										      ? moment(earliest[0]).format("HH:mm:ss")
										      : ""
								      },
								      {
									      "index": serverPingsCount < 11 ? 17 : 13,
									      "label": "Now"
								      }
							      ]}
							      style={{fontSize: "12px"}}/>

							<Label align="center" size="small">lowest: {low[1]}ms</Label>
							{/*<Box
								direction="row"
								justify="between"
								align="center"
								pad={{between: "small"}}
								responsive={false}
							>
								<Label size="small">lowest: {low[1]}ms</Label>
								<Label size="small">highest: {high[1]}ms</Label>
							</Box>*/}
						</Chart>
					</Chart>
				</FadeAnimate>
				{this.state.confirmClear
					? this.confirmClear()
					: null}
			</Box>
		)
	}
}

ServerMeter.propTypes = {
	servers: PropTypes.object.isRequired,
	pings: PropTypes.object.isRequired,
	ui: PropTypes.object.isRequired,
	serverToggle: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
	servers: state.servers,
	pings: state.pings,
	ui: state.ui
})

const mapDispatchToProps = dispatch => bindActionCreators({
	serverToggle,
	deleteServer,
	resetServerPing
}, dispatch)

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ServerMeter)
