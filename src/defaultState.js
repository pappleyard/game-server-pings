const defaultState = {
	ui: {
		allServersView: "list",
		featuredServer: null,
		activeProfile: "Default",
		initialised: false,
		newServerModal: false,
		newServerData: {},
		newServersList: [],
		toasty: {},
		layout: "split",
		isSmall: false,
	},
	
	servers: window.defaultServers,
	
	pings: {
		profiles: ["Default"],
		current: [],
		
		servers: null
	}
}

export default defaultState
