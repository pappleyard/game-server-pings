import * as types from "./types"

import {getDateTimeString} from "../lib/funcs"

export const toggleListView = (view = "toggle") => ({
	type: types.ALL_SERVERS_VIEW,
	payload: view
})

export const recordServerPing = (server, ping) => ({
	type: types.RECORD_SERVER_PING,
	payload: {
		server: server,
		stamp: getDateTimeString(),
		ping: parseInt(ping, 10)
	}
})

export const currentPingServer = (server, flag = true) => ({
	type: types.PING_CURRENT,
	payload: {
		server: server,
		status: flag
	}
})


export const resetServerPing = (server) => ({
	type: types.RESET_SERVER_PING,
	payload: server
})

export const setFeaturedServer = (server) =>
{
	console.log("setting featured server:", server)
	return {
		type: types.SERVER_FEATURED,
		payload: {server: server}
	}
}

export const serverToggle = (server) =>
{
	return {
		type: types.SERVER_TOGGLE,
		payload: {server: server}
	}
}

export const serverInputToggle = (view = "toggle") =>
{
	return {
		type: types.SERVER_INPUT_TOGGLE,
		payload: view
	}
}

export const addCustomServer = (name, address, subtitle) =>
{
	return {
		type: types.SERVER_ADD,
		payload: [name, address, subtitle]
	}
}

export const deleteServer = (name) =>
{
	return {
		type: types.SERVER_REMOVE,
		payload: name
	}
}

export const layoutToggle = (layout = "split") =>
{
	return {
		type: types.LAYOUT_TOGGLE,
		payload: layout
	}
}

export const setIsSmall = (small) =>
{
	return {
		type: types.IS_SMALL,
		payload: small
	}
}

export const resetServers = () =>
{
	return {
		type: types.SERVERS_RESET,
		payload: window.defaultServers
	}
}