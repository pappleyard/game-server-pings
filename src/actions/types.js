/*

We need to:

	- Enable a server
	- Disable a server
	- Add a custom endpoint
	- Remove a custom endpoint

	- Toggle 'all servers' view between list and graph
	- Set active server
	
	- Pause a server
	- Unpause a server
	
	- Set active game

 */

/// UI
export const ALL_SERVERS_VIEW = "ALL_SERVERS_VIEW" // Can be list or graph
export const SERVER_FEATURED = "SERVER_FEATURED"
export const SERVER_INPUT_TOGGLE = "SERVER_INPUT_TOGGLE"
export const LAYOUT_TOGGLE = "LAYOUT_TOGGLE"
export const IS_SMALL = "IS_SMALL"

/// PINGS
export const PING_CURRENT = "PING_CURRENT"
export const RECORD_SERVER_PING = "RECORD_SERVER_PING"
export const RESET_SERVER_PING = "RESET_SERVER_PING"

/// Servers
export const SERVER_TOGGLE = "SERVER_TOGGLE"
export const SERVER_ADD = "SERVER_ADD"
export const SERVER_REMOVE = "SERVER_REMOVE"
export const SERVERS_RESET = "SERVERS_RESET"