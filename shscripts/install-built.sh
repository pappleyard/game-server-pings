#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
PARENT_DIR="$(dirname "$SCRIPT_DIR")"
EXCLUDE_FILE=$SCRIPT_DIR/exclude-list.txt
BUILD_DIR=$PARENT_DIR/build
PUBLIC_DIR=$PARENT_DIR/public

cd $PARENT_DIR

node node_modules/npm-run-all/bin/npm-run-all build-css build-js

cd $PARENT_DIR

cp -r build/static ../gsp-laravel/site/public/


exit 0