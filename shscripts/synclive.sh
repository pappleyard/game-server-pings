#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
PARENT_DIR="$(dirname "$SCRIPT_DIR")"
EXCLUDE_FILE=$SCRIPT_DIR/exclude-list.txt
BUILD_DIR=$PARENT_DIR/build
PUBLIC_DIR=$PARENT_DIR/public

rsync -zvaP --delete --chown=www-data:www-data --exclude-from=$SCRIPT_DIR/exclude-list-build.txt $PARENT_DIR/* root@199.241.28.21:/var/www/gameserverping.com/

rsync -zvaP --delete --chown=www-data:www-data $BUILD_DIR/* root@199.241.28.21:/var/www/gameserverping.com/public

#rsync -zvaP --delete --chown=www-data:www-data $PUBLIC_DIR/*.php root@199.241.28.21:/var/www/gameserverping.com/public

ssh -o IdentitiesOnly=yes root@199.241.28.20 "chown -R www-data:www-data /var/www/gameserverping.com"

exit 0