#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
PARENT_DIR="$(dirname "$SCRIPT_DIR")"
EXCLUDE_FILE=$SCRIPT_DIR/exclude-list.txt
BUILD_DIR=$PARENT_DIR/build
PUBLIC_DIR=$PARENT_DIR/public

#rsync -zvaP --delete --exclude-from=$SCRIPT_DIR/exclude-list-vagrant.txt $PARENT_DIR/* -e "ssh -p 2222" vagrant@127.0.0.1:/var/www/gsp/site

#rsync -zvaP -e "ssh -p 2222" --exclude "images" $BUILD_DIR/* vagrant@127.0.0.1:/var/www/gsp/site/public

#rsync -zvaP --delete --chown=www-data:www-data $PUBLIC_DIR/*.php vagrant@127.0.0.1:/var/www/gameserverping.com/public

#ssh -p 2222 vagrant@127.0.0.1 "chown -R www-data:www-data /var/www/gameserverping.com"

scp $BUILD_DIR/static/js/* ../../gsp-laravel/site/public/static/js
#scp $BUILD_DIR/static/css/* ../../gsp-laravel/site/public/static/css

exit 0